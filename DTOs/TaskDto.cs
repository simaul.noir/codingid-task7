namespace TaskDay7.DTOs
{
    public class TaskDto
    {
        public string task_detail { get; set; } = string.Empty;
    }

    public class TaskInput
    {
        public string name { get; set; } = string.Empty;
        public List<TaskDto> tasks { get; set; }
    }
}