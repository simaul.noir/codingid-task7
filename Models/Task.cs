using TaskDay7.DTOs;
using TaskDay7.Data;

namespace TaskDay7.Models
{
    public class Task
    {
        public int pk_tasks_id { get; set; }
        public string task_detail { get; set; } = string.Empty;
        public int fk_users_id { get; set; }
    }

    public class TaskGet
    {
        public int pk_tasks_id { get; set; }
        public string task_detail { get; set; } = string.Empty;
    }

    public class TaskAll
    {
        public List<TaskGet> tasks { get; set; }
        public int pk_users_id { get; set; }
        public string name { get; set; } = string.Empty;
    }
}