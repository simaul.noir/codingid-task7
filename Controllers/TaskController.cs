using TaskDay7.Data;
using TaskDay7.DTOs;
using TaskDay7.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace TaskDay7.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly TaskData _taskData;
        private readonly UserData _userData;
        public TaskController(TaskData taskData, UserData userData)
        {
            _taskData = taskData;
            _userData = userData;
        }

        [HttpGet("GetUserWithTask")]
        public IActionResult GetAll([FromQuery] string? name)
        {
            if (!String.IsNullOrEmpty(name))
            {
                try
                {
                    TaskAll? tasks = _taskData.GetByName(name);

                    if (tasks == null)
                    {
                        return NotFound($"Data {name} is not found");
                    }

                    return Ok(tasks);
                }
                catch(Exception ex)
                {
                    return StatusCode(500, ex.Message);
                }
            }
            else
            {
                try
                {
                    List<TaskAll> tasks = _taskData.GetAll();

                    return Ok(tasks);
                }
                catch(Exception ex)
                {
                    return StatusCode(500, ex.Message);
                }
    
            }
        }

        [HttpPost("AddUserWithTask")]
        public IActionResult Post([FromBody] TaskInput tasks)
        {
            try
            {
                UserDto user = new UserDto
                {
                    name = tasks.name,
                };

                bool resultUser = _userData.InputUser(user);
                bool resultTask = _taskData.InsertTask(tasks.tasks);

                if (resultUser && resultTask)
                {
                    return StatusCode(201, "Data Was Successfully Inserted");
                }
                else
                {
                    return StatusCode(500, "Data Not Inserted!");
                }

            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
