using TaskDay7.Models;
using TaskDay7.DTOs;
using System.Data;
using System.Data.SqlClient;

namespace TaskDay7.Data
{
    public class TaskData
    {
        private readonly string ConnectionString;
        private readonly IConfiguration _configuration;

        public TaskData(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionString = _configuration.GetConnectionString("Default Connection");
        }

        public List<TaskAll> GetAll()
        {
            List<TaskAll> allTasks = new List<TaskAll>();

            int current_idx = 1;
            int last_idx;

            string query_idx = $"SELECT IDENT_CURRENT('Users')";

            using(SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using(SqlCommand command = new SqlCommand(query_idx, connection))
                {
                    connection.Open();

                    last_idx = Convert.ToInt32(command.ExecuteScalar());

                    connection.Close();
                }
            }
            for(int i = current_idx; i <= last_idx; i++)
            {   
                List<TaskGet> tasks = new List<TaskGet>();
                string query_task = $"SELECT pk_tasks_id, task_detail FROM Tasks WHERE fk_users_id = {i}";
                string query_user = $"SELECT pk_users_id, name FROM Users WHERE pk_users_id = {i}";

                using(SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using(SqlCommand command = new SqlCommand(query_task, connection))
                    {
                        connection.Open();
                        using(SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                tasks.Add(new TaskGet
                                {
                                    pk_tasks_id = Convert.ToInt32(reader["pk_tasks_id"]),
                                    task_detail = reader["task_detail"].ToString() ?? string.Empty
                                });
                            }
                        }
                        connection.Close();
                    }
                }
                using(SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    using(SqlCommand command = new SqlCommand(query_user, connection))
                    {
                        try
                        {
                            connection.Open();

                            using(SqlDataReader reader = command.ExecuteReader())
                            {
                                while(reader.Read())
                                {
                                    allTasks.Add(new TaskAll
                                    {
                                        tasks = tasks,
                                        pk_users_id = Convert.ToInt32(reader["pk_users_id"]),
                                        name = reader["name"].ToString() ?? string.Empty
                                    });
                                }
                            }
                        }
                        catch
                        {
                            throw;
                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }
                current_idx++;
            }

            return allTasks;
        }

        public TaskAll? GetByName(string? name)
        {
            TaskAll? tasks = null;
            List<TaskGet> taskList = new List<TaskGet>();

            int pk_users_id = 0;
            string user_name = "";

            string query_user = $"SELECT pk_users_id, name FROM Users WHERE name = @name";
            string query_task = $"SELECT pk_tasks_id, task_detail FROM Tasks WHERE fk_users_id = {pk_users_id}";

            using(SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using(SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        command.Connection = connection;
                        command.CommandText = query_user;

                        command.Parameters.Clear();

                        command.Parameters.AddWithValue("@name", name);

                        connection.Open();

                        using(SqlDataReader reader = command.ExecuteReader())
                        {
                            while(reader.Read())
                            {
                                pk_users_id = Convert.ToInt32(reader["pk_users_id"]);
                                user_name = reader["name"].ToString() ?? string.Empty;
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            using(SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using(SqlCommand command = new SqlCommand(query_task, connection))
                {
                    try
                    {
                        connection.Open();

                        using(SqlDataReader reader = command.ExecuteReader())
                        {
                            while(reader.Read())
                            {
                                taskList.Add(new TaskGet
                                {
                                    pk_tasks_id = Convert.ToInt32(reader["pk_tasks_id"]),
                                    task_detail = reader["task_detail"].ToString() ?? string.Empty,
                                });
                            }
                        }
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            tasks = new TaskAll
            {
                tasks = taskList,
                pk_users_id = pk_users_id,
                name = user_name,
            };
            return tasks;
        }

        public bool InsertTask(List<TaskDto> tasks)
        {
            bool result = false;

            int user_id;

            string query = $"INSERT INTO Tasks (task_detail, fk_users_id) VALUES (@task_detail, @fk_users_id)";
            string query_users_id = $"SELECT IDENT_CURRENT('Users')";
            
            using(SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using(SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = query_users_id;

                    connection.Open();

                    user_id = Convert.ToInt32(command.ExecuteScalar());

                    connection.Close();
                }
                foreach(var task in tasks)
                {
                    {
                        using(SqlCommand command = new SqlCommand())
                        {
                            command.Connection = connection;
                            command.CommandText = query;

                            command.Parameters.Clear();

                            command.Parameters.AddWithValue("@task_detail", task.task_detail);
                            command.Parameters.AddWithValue("@fk_users_id", user_id);

                            try
                            {
                                connection.Open();
                                result = command.ExecuteNonQuery() > 0 ? true : false;
                            }
                            catch
                            {
                                throw;
                            }
                            finally
                            {
                                connection.Close();
                            }
                        }
                    }
                }
            }
            return result;
        }

    }
}