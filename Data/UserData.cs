using TaskDay7.Models;
using TaskDay7.DTOs;
using System.Data;
using System.Data.SqlClient;

namespace TaskDay7.Data
{
    public class UserData
    {
        private readonly string ConnectionString;
        private readonly IConfiguration _configuration;

        public UserData(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionString = configuration.GetConnectionString("Default Connection");
        }

        public bool InputUser(UserDto userDto)
        {
            bool result = false;

            string query = $"INSERT INTO Users(name) VALUES (@name)";

            using(SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using(SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = query;

                    command.Parameters.Clear();

                    command.Parameters.AddWithValue("@name", userDto.name);

                    try
                    {
                        connection.Open();
                        result = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return result;
        }
    }
}